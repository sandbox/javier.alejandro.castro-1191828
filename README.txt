
-- SUMMARY --

File protect is a File field formatter, intended to protect file downloads, allowing to download only to 
authenticated users. It works by replacing direct links to files on the web server to custom links 
handled by the module itself, which permits handling this case (allowing download of file by registered 
clients for the website). May also be complemented by a web server configuration to disallow direct 
URL downloads.

-- INSTALL --

* Install as usual.
  See http://drupal.org/documentation/install/modules-themes/modules-7 for further information.
* Enable the module on /admin/modules, under Fields category.

-- CONFIGURATION --

* Go to Content type managment, on /admin/structure/types
* Click on Manage display for the desired content type
* For the File field to protect from unauthenticated downloads, choose Format 'Protect file (authenticated users)'
* Save

-- USAGE --

After configuring, the formatter will display files as, for example, http://example.com/download/(base64_encoding)
The module handles the base64 encoding to identify the download and protect it from unauthorized access.

-- CONTACT --

Current maintainers: 
 Javier A. Castro - http://drupal.org/user/482562